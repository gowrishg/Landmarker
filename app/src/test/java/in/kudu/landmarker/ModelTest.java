package in.kudu.landmarker;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import in.kudu.landmarker.data.Coordinates;

/**
 * Created by gowrishg on 19/10/17.
 */

@RunWith(JUnit4.class)
public class ModelTest {

    @Test
    public void checkCoordinates() {
        Coordinates coordinates = new Coordinates();
        coordinates.latitude = 1.3;
        coordinates.longitude = 103.3;
        Assert.assertEquals("1300000_103300000", coordinates.getUuid());

        coordinates.latitude = 1.312341234;
        coordinates.longitude = 103.312341234;
        Assert.assertEquals("1312341_103312341", coordinates.getUuid());

        coordinates.latitude = 1.31238888;
        coordinates.longitude = 103.312348888;
        Assert.assertEquals("1312389_103312349", coordinates.getUuid());

    }
}
