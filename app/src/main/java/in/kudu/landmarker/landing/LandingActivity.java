package in.kudu.landmarker.landing;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import in.kudu.landmarker.R;
import in.kudu.landmarker.ViewModelFactory;
import in.kudu.landmarker.maps.MapsActivity;

public class LandingActivity extends AppCompatActivity {

    private EditText usernameEditText;
    private Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        usernameEditText = (EditText) findViewById(R.id.editText);
        saveButton = (Button) findViewById(R.id.saveButton);

        ViewModelFactory factory = ViewModelFactory.getInstance(getApplication());
        final LandingViewModel landingViewModel = ViewModelProviders.of(this, factory).get(LandingViewModel.class);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = usernameEditText.getText().toString();
                if (landingViewModel.isValid(username)) {
                    landingViewModel.setUserName(username);
                    landingViewModel.getAllNotes(username);
                    Intent mapActivityIntent = new Intent(LandingActivity.this, MapsActivity.class);
                    startActivity(mapActivityIntent);
                    finish();
                } else {
                    Toast.makeText(LandingActivity.this, R.string.valid_username_msg, Toast.LENGTH_SHORT).show();
                }
            }
        });

        if (landingViewModel != null) {
            usernameEditText.setText(landingViewModel.getUser().name);
        }
    }
}
