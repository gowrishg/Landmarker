package in.kudu.landmarker.landing;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import in.kudu.landmarker.data.NoteRepository;
import in.kudu.landmarker.data.User;
import in.kudu.landmarker.data.UserRepository;

/**
 * Created by gowrishg on 15/10/17.
 */

public class LandingViewModel extends AndroidViewModel {

    UserRepository mUserRepository;
    NoteRepository mNoteRepository;
    public LandingViewModel(Application context, UserRepository userRepository, NoteRepository noteRepository) {
        super(context);
        mUserRepository = userRepository;
        mNoteRepository = noteRepository;
    }

    @Nullable
    private User mUser;

    @Nullable
    public User getUser() {
        mUser = mUserRepository.getUser();
        return mUser;
    }

    /**
     * Sets the user name
     * @param userName
     */
    public void setUserName(final String userName) {
        if (!TextUtils.isEmpty(userName)) {
            this.mUser = new User();
            this.mUser.name = userName.trim();
            mUserRepository.setUser(this.mUser);
        }
    }

    /**
     * Checks if the input is valid or not
     * @param username
     * @return true if it's valid
     */
    public boolean isValid(String username) {
        if (TextUtils.isEmpty(username)) {
            username = "";
        }
        return username.trim().length() > 0;
    }

    /**
     * Fetched all data for the user
     * @param username
     */
    public void getAllNotes(String username) {
        //mNoteRepository.getAllNotesOnFirstLoad();
    }
}
