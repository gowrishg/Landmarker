package in.kudu.landmarker;

import android.annotation.SuppressLint;
import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.persistence.room.Room;
import android.support.annotation.VisibleForTesting;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import in.kudu.landmarker.data.NoteDataSourceLocal;
import in.kudu.landmarker.data.NoteDataSourceRemote;
import in.kudu.landmarker.data.NoteDatabase;
import in.kudu.landmarker.data.NoteRepository;
import in.kudu.landmarker.data.UserRepository;
import in.kudu.landmarker.landing.LandingViewModel;
import in.kudu.landmarker.maps.MapsViewModel;
import in.kudu.landmarker.notes.NotesViewModel;
import in.kudu.landmarker.search.SearchViewModel;

/**
 * Created by gowrishg on 16/10/17.
 */

public class ViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    @SuppressLint("StaticFieldLeak")
    private static volatile ViewModelFactory instance;

    private final Application mApplication;

    private final UserRepository mUserRepository;
    private final NoteRepository mNoteRepository;

    private ViewModelFactory(Application application, UserRepository userRepository, NoteRepository noteRepository) {
        this.mApplication = application;
        this.mUserRepository = userRepository;
        this.mNoteRepository = noteRepository;
    }

    /**
     * Singleton class helps to prepare an instance of the ViewModel factory
     * @param application - only application context is allowed.
     * @return
     */
    public static ViewModelFactory getInstance(Application application) {
        if (instance == null) {
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference userDatabaseReference = firebaseDatabase.getReference("user_cache");
            UserRepository userRepository = new UserRepository(application, userDatabaseReference);
            NoteDatabase noteDatabase = Room.databaseBuilder(application.getApplicationContext(), NoteDatabase.class, "NoteDatabase.db").build();
            NoteDataSourceLocal noteDataSourceLocal = new NoteDataSourceLocal(noteDatabase);
            DatabaseReference firebaseDatabaseReference = firebaseDatabase.getReference("RemoteNoteDatabase");
            NoteDataSourceRemote noteDataSourceRemote = new NoteDataSourceRemote(firebaseDatabaseReference);
            NoteRepository noteRepository = new NoteRepository(application, userRepository, noteDataSourceLocal, noteDataSourceRemote);
            instance = new ViewModelFactory(application, userRepository, noteRepository);
        }
        return instance;
    }

    @VisibleForTesting
    public static void destroyInstance() {
        instance = null;
    }

    /**
     * Class retrieves the ViewModel
     *
     * @param modelClass
     * @param <T>
     * @return
     */
    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(LandingViewModel.class)) {
            return (T) new LandingViewModel(mApplication, mUserRepository, mNoteRepository);
        } else if (modelClass.isAssignableFrom(MapsViewModel.class)) {
            return (T) new MapsViewModel(mApplication, mUserRepository, mNoteRepository);
        } else if (modelClass.isAssignableFrom(NotesViewModel.class)) {
            return (T) new NotesViewModel(mApplication, mUserRepository, mNoteRepository);
        } else if (modelClass.isAssignableFrom(SearchViewModel.class)) {
            return (T) new SearchViewModel(mApplication, mNoteRepository);
        }
        return super.create(modelClass);
    }
}
