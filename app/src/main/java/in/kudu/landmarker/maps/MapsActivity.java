package in.kudu.landmarker.maps;

import android.Manifest;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import in.kudu.landmarker.R;
import in.kudu.landmarker.ViewModelFactory;
import in.kudu.landmarker.data.Coordinates;
import in.kudu.landmarker.notes.NotesActivity;
import in.kudu.landmarker.search.SearchActivity;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback,
        ActivityCompat.OnRequestPermissionsResultCallback, com.google.android.gms.location.LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final int MY_LOCATION_REQUEST_CODE = 100;
    private static final float DEFAULT_ZOOM = 1;
    private GoogleMap mMap;
    MapsViewModel mMapsViewModel;
    Observer<List<Coordinates>> mCoordinatesObserver;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Marker mCurrLocationMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        android.support.v7.widget.Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        myToolbar.inflateMenu(R.menu.main_menu);

        ViewModelFactory factory = ViewModelFactory.getInstance(getApplication());
        mMapsViewModel = ViewModelProviders.of(this, factory).get(MapsViewModel.class);

        FloatingActionButton addNoteFab = (FloatingActionButton) findViewById(R.id.addNoteFab);
        addNoteFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mMapsViewModel.isValidLocation()) {
                    showNoteDialog();
                } else {
                    showLocationDialog();
                }
            }
        });
        FloatingActionButton currentLocFab = (FloatingActionButton) findViewById(R.id.currentLocFab);
        currentLocFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enableMyLocation();
            }
        });

        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        subscribeNotesInMap();

        mMapsViewModel.loadLocations();
    }

    /**
     * observe notes and update UI when changed
     */
    private void subscribeNotesInMap() {
        mCoordinatesObserver = new Observer<List<Coordinates>>() {
            @Override
            public void onChanged(@Nullable List<Coordinates> coordinates) {
                mMap.clear();
                for (Coordinates coordinate : coordinates) {
                    MarkerOptions markerOptions = new MarkerOptions().position(new LatLng(coordinate.latitude, coordinate.longitude));
                    Marker marker = mMap.addMarker(markerOptions);
                    marker.setTag(coordinate);
                }

                mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        if (marker.getTag() instanceof Coordinates) {
                            Coordinates coord = (Coordinates) marker.getTag();
                            Intent mapActivityIntent = new Intent(MapsActivity.this, NotesActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putDouble("latitude", coord.latitude);
                            bundle.putDouble("longitude", coord.longitude);
                            mapActivityIntent.putExtras(bundle);
                            startActivity(mapActivityIntent);
                        }
                        return false;
                    }
                });
            }
        };
        mMapsViewModel.getLocations().observeForever(mCoordinatesObserver);
    }

    @Override
    protected void onDestroy() {
        mMapsViewModel.getLocations().removeObserver(mCoordinatesObserver);
        super.onDestroy();
    }

    /**
     * Informs user that there is no valid location
     */
    private void showLocationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.invalid_location_title);
        builder.setMessage(R.string.invalid_location_msg);
        builder.setPositiveButton(R.string.ok_button, null);
        builder.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * UI to enter notes
     */
    private void showNoteDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        builder.setTitle(R.string.add_notes_title);
        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String note = input.getText().toString();
                if (mMapsViewModel.isValid(note)) {
                    mMapsViewModel.addNote(note);
                }
            }
        });
        builder.setNegativeButton("Close", null);
        builder.show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        enableMyLocation();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    /**
     * Check if location service is enabled; else notify user to enable it.
     */
    public void enableMyLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            mMap.setMyLocationEnabled(true);
            mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                @Override
                public void onMyLocationChange(Location location) {
                    updateCurrentLocation(location, false);
                }
            });

            Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);
            updateCurrentLocation(lastLocation, true);

        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.enable_location_title);
            builder.setMessage(R.string.enale_permission_msg);
            builder.setPositiveButton(R.string.allow_string, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    ActivityCompat.requestPermissions(MapsActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_LOCATION_REQUEST_CODE);
                }
            });
            builder.setNegativeButton(R.string.cancel_string, null);
            builder.show();
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == MY_LOCATION_REQUEST_CODE) {
            if (permissions.length == 1 &&
                    permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                enableMyLocation();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_main_search:
                Intent searchActivityIntent = new Intent(MapsActivity.this, SearchActivity.class);
                startActivity(searchActivityIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        updateCurrentLocation(location, false);
    }

    /**
     * Draws a PIN at current location
     * @param location
     * @param centerMap centers map if set to true
     */
    private void updateCurrentLocation(@Nullable Location location, boolean centerMap) {
        if (location != null) {

            if (mCurrLocationMarker != null) {
                mCurrLocationMarker.remove();
            }

            //Place current location marker
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
            mCurrLocationMarker = mMap.addMarker(markerOptions);

            if(centerMap) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                        new LatLng(location.getLatitude(),
                                location.getLongitude()), mMap.getMaxZoomLevel() - 2));
            }
            mMapsViewModel.setCurrentLocation(location);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
