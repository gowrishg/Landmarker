package in.kudu.landmarker.maps;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.location.Location;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import in.kudu.landmarker.data.Coordinates;
import in.kudu.landmarker.data.Note;
import in.kudu.landmarker.data.NoteDataSource;
import in.kudu.landmarker.data.NoteRepository;
import in.kudu.landmarker.data.UserRepository;

/**
 * Created by gowrishg on 16/10/17.
 */

public class MapsViewModel extends AndroidViewModel {

    private final NoteRepository mNoteRepository;
    private final UserRepository mUserRepository;
    MutableLiveData<List<Coordinates>> locations;
    private Location currentLocation;

    public MapsViewModel(Application application, UserRepository userRepository, NoteRepository noteRepository) {
        super(application);
        this.mNoteRepository = noteRepository;
        this.mUserRepository = userRepository;
    }

    /**
     * Adds a new note
     * @param note
     */
    public void addNote(String note) {
        Note noteObj = new Note();
        noteObj.coordinates = new Coordinates();
        noteObj.coordinates.latitude = currentLocation.getLatitude();
        noteObj.coordinates.longitude = currentLocation.getLongitude();
        noteObj.userName = mUserRepository.getUserName();
        noteObj.id = UUID.randomUUID() + noteObj.userName;
        noteObj.note = note;
        mNoteRepository.addNote(noteObj);

        List<Coordinates> coordinatesList = getLocations().getValue();
        if (coordinatesList == null) {
            coordinatesList = new ArrayList<>();
        }
        coordinatesList.add(noteObj.coordinates);
        getLocations().postValue(coordinatesList);
    }

    /**
     * Checks if the input is valid or not
     * @param note
     * @return true if it's valid
     */
    public boolean isValid(String note) {
        if (TextUtils.isEmpty(note)) {
            note = "";
        }
        return note.trim().length() > 0;
    }

    /**
     * Sets the current location
     * @param currentLocation
     */
    public void setCurrentLocation(@Nullable Location currentLocation) {
        this.currentLocation = currentLocation;
    }

    /**
     * Checks if a location is valid or not
     * @return true if valid
     */
    public boolean isValidLocation() {
        return currentLocation != null;
    }

    /**
     * Gets reference of location which may be observed by the UI elements.
     * @return coordinates live data
     */
    public MutableLiveData<List<Coordinates>> getLocations() {
        if (locations == null) {
            locations = new MutableLiveData<>();
        }
        return locations;
    }

    /**
     * Prepare data which will be shown in UI
     */
    public void loadLocations() {
        mNoteRepository.getLocations(new NoteDataSource.LocationsCallback() {
            @Override
            public void onDataAvailable(List<Coordinates> coordinates) {
                getLocations().postValue(coordinates);
            }

            @Override
            public void onDataNotAvailable() {
                getLocations().postValue(new ArrayList<Coordinates>(0));
            }
        });
    }


}
