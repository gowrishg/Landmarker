package in.kudu.landmarker.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.firebase.database.DatabaseReference;

/**
 * Created by gowrishg on 16/10/17.
 */
public class UserRepository {

    User mUser;
    Context mContext;
    DatabaseReference myRef;

    public UserRepository(Context context, DatabaseReference firebaseDatabaseReference) {
        this.mContext = context;
        myRef = firebaseDatabaseReference;
    }

    /**
     * Gets the user information
     * @return User object
     */
    public User getUser() {
        if (mUser == null || TextUtils.isEmpty(mUser.name)) {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences("user_cache", Context.MODE_PRIVATE);
            mUser = new User();
            mUser.name = sharedPreferences.getString("user_name", "");
        }
        return mUser;
    }

    /**
     * Sets the user profile information
     * @param userName
     */
    public void setUser(User userName) {
        this.mUser = userName;

        SharedPreferences sharedPreferences = mContext.getSharedPreferences("user_cache", Context.MODE_PRIVATE);
        sharedPreferences.edit().putString("user_name", this.mUser.name).apply();
        myRef.child(userName.name).setValue(userName);
    }

    public String getUserName() {
        return this.getUser().name;
    }
}
