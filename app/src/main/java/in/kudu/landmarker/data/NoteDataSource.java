package in.kudu.landmarker.data;

import java.util.List;

/**
 * Created by gowrishg on 16/10/17.
 */

public interface NoteDataSource {

    // callback helps to obtain list of notes and let know if no data is available
    interface NotesCallback {
        /**
         * callback once notes are available
         * @param note
         */
        void onDataAvailable(List<Note> note);

        /**
         * callback to notify that there is no data available
         */
        void onDataNotAvailable();
    }

    // callback helps to obtain list of coordinates and let know if no data is available
    interface LocationsCallback {
        void onDataAvailable(List<Coordinates> coordinates);

        void onDataNotAvailable();
    }

    /**
     * API to fetch the list of notes
     * @param userName - which user to fetch data for
     * @param notesCallback
     */
    void getNotes(String userName, NotesCallback notesCallback);

    /**
     * API to fetch the list of notes
     * @param coordinates - based on a particular location
     * @param userName - based on a username
     * @param notesCallback
     */
    void getNotes(Coordinates coordinates, String userName, NotesCallback notesCallback);

    /**
     * API to fetch the list of notes
     * @param coordinates - based on a particular location
     * @param notesCallback
     */
    void getNotes(Coordinates coordinates, NotesCallback notesCallback);

    /**
     * Insert a new note for a given location
     * @param note
     */
    void addNote(Note... note);

    /**
     * API to search based on the keyword.
     * TODO: It doesn't support out of sequence search.
     * @param keyword
     * @param notesCallback
     */
    void searchByNameOrNotes(String keyword, NotesCallback notesCallback);

    /**
     * API to fetch coordinates where notes are already created
     * @param userName
     * @param locationsCallback
     */
    void getLocations(String userName, LocationsCallback locationsCallback);
}
