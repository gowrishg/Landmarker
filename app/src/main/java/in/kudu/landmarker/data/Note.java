package in.kudu.landmarker.data;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by gowrishg on 16/10/17.
 */
@Entity
public class Note {
    @PrimaryKey
    public String id;
    public String userName;
    public String note;
    @Embedded
    public Coordinates coordinates;
}
