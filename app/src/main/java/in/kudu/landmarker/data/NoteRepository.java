package in.kudu.landmarker.data;

import android.app.Application;
import android.content.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gowrishg on 16/10/17.
 */

public class NoteRepository {

    private final UserRepository mUserRepository;
    private final Context mContext;
    private final NoteDataSourceLocal mNoteDataSourceLocal;
    private final NoteDataSourceRemote mNoteDataSourceRemote;

    public NoteRepository(Application application, UserRepository userRepository, NoteDataSourceLocal noteDataSourceLocal, NoteDataSourceRemote noteDataSourceRemote) {
        this.mContext = application.getApplicationContext();
        this.mUserRepository = userRepository;
        this.mNoteDataSourceLocal = noteDataSourceLocal;
        this.mNoteDataSourceRemote = noteDataSourceRemote;
    }

    /**
     * Adds note to the local db and server
     * @param noteObj
     */
    public void addNote(Note noteObj) {
        mNoteDataSourceLocal.addNote(noteObj);
        mNoteDataSourceRemote.addNote(noteObj);
    }

    /**
     * Retrieves all location which will be got from local db and server
     * @param locationsCallback
     */
    public void getLocations(NoteDataSource.LocationsCallback locationsCallback) {
        mNoteDataSourceLocal.getLocations(mUserRepository.getUserName(), locationsCallback);
        mNoteDataSourceRemote.getLocations(mUserRepository.getUserName(), locationsCallback);
    }

    /**
     * Search for a note based on contained text or username
     * @param keyword
     * @param notesCallback Call back once the data is ready to serve
     */
    public void search(String keyword, NoteDataSource.NotesCallback notesCallback) {
        keyword = "%" + keyword.replaceAll("\\s+", "%") + "%";
        mNoteDataSourceLocal.searchByNameOrNotes(keyword, notesCallback);
        mNoteDataSourceRemote.searchByNameOrNotes(keyword, notesCallback);
    }

    /**
     * Retrieves notes that are saved at a particular location
     * @param coordinates
     * @param notesCallback
     */
    public void getNotes(Coordinates coordinates, final NoteDataSource.NotesCallback notesCallback) {
        mNoteDataSourceLocal.getNotes(coordinates, notesCallback);

        //TODO: it's more optimum to perform this operation at the server end.
        final List<Note> notes = new ArrayList<>();
        mNoteDataSourceRemote.getNotes(coordinates, new NoteDataSource.NotesCallback() {
            @Override
            public void onDataAvailable(List<Note> note) {
                notes.addAll(note);
                notesCallback.onDataAvailable(notes);
            }

            @Override
            public void onDataNotAvailable() {
                // do nothing
            }
        });
    }

    /**
     * Fetches all data from server for the existing user.
     * This helps to keep the devices in sync with the user data
     */
    public void getAllNotesOnFirstLoad() {
        mNoteDataSourceRemote.getNotes(mUserRepository.getUser().name, new NoteDataSource.NotesCallback() {
            @Override
            public void onDataAvailable(List<Note> notes) {
                for (Note note : notes) {
                    mNoteDataSourceLocal.addNote(note);
                }
            }

            @Override
            public void onDataNotAvailable() {

            }
        });
    }
}
