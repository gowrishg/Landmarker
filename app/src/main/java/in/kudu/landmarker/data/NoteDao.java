package in.kudu.landmarker.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by gowrishg on 16/10/17.
 */
@Dao
public interface NoteDao {
    @Query("SELECT * FROM Note where round(latitude, 6)=round(:latitude,6) and round(longitude, 6)=round(:longitude,6) and userName like :userName")
    List<Note> getNotes(double latitude, double longitude, String userName);

    @Insert(onConflict = REPLACE)
    void addNote(Note... note);

    @Query("SELECT * FROM Note WHERE username like :keyword or note like :keyword")
    List<Note> searchByNameOrNotes(String keyword);

    @Query("Select * from Note where round(latitude, 6)=round(:latitude,6) and round(longitude, 6)=round(:longitude,6)")
    List<Note> getNotes(double latitude, double longitude);

    @Query("Select * from Note where userName=:userName group by round(latitude, 6), round(longitude, 6)")
    List<Coordinates> getLocations(String userName);
}
