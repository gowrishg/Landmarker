package in.kudu.landmarker.data;

import android.support.annotation.WorkerThread;
import android.text.TextUtils;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gowrishg on 16/10/17.
 */

public class NoteDataSourceRemote implements NoteDataSource {

    private final DatabaseReference myRef;
    public NoteDataSourceRemote(DatabaseReference firebaseDatabaseReference) {
        myRef = firebaseDatabaseReference;
    }

    @Override
    public void getNotes(final String userName, final NotesCallback notesCallback) {
        final Query myTopPostsQuery = myRef.child("by_user").child(userName);
        myTopPostsQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    String id = postSnapshot.getKey().toString();
                    getNotesFromOnline(id, notesCallback);
                }
                myTopPostsQuery.removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                myTopPostsQuery.removeEventListener(this);
                if (notesCallback != null) {
                    notesCallback.onDataNotAvailable();
                }
            }
        });
    }

    /**
     * Fetched details about the node based on the ID
     * @param id
     * @param notesCallback
     */
    @WorkerThread
    private void getNotesFromOnline(String id, final NotesCallback notesCallback) {
        final Query myTopPostsQuery = myRef.child("by_id").child(id);
        myTopPostsQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Note note = dataSnapshot.getValue(Note.class);
                myTopPostsQuery.removeEventListener(this);
                if (notesCallback != null) {
                    List<Note> notes = new ArrayList<Note>();
                    notes.add(note);
                    notesCallback.onDataAvailable(notes);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                myTopPostsQuery.removeEventListener(this);
            }
        });
    }

    @Override
    public void getNotes(Coordinates coordinates, String userName, NotesCallback notesCallback) {
        throw new UnsupportedOperationException("Yet to be implemented");
    }

    @Override
    public void getNotes(final Coordinates coordinates, final NotesCallback notesCallback) {
        final Query myTopPostsQuery = myRef.child("by_location").child(coordinates.getUuid()).child("notes");
        myTopPostsQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    String id = postSnapshot.getKey();
                    getNotesFromOnline(id, notesCallback);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                notesCallback.onDataNotAvailable();
            }
        });
    }

    @Override
    public void addNote(Note... notes) {
        //! the ID holds the entire note data. The following structure created to comply with the way the data is retrieved.
        for (Note note : notes) {
            myRef.child("by_id").child(note.id).setValue(note);
            myRef.child("by_user").child(note.userName).child(note.id).setValue(note.id);
            myRef.child("by_location").child(note.coordinates.getUuid()).child("notes").child(note.id).setValue(note.id);
            myRef.child("by_location").child(note.coordinates.getUuid()).child("coordinate").setValue(note.coordinates);
        }
    }

    @Override
    public void searchByNameOrNotes(final String keyword, final NotesCallback notesCallback) {
        //! TODO: not an efficient way to search; as it's better to be done at the server end.
        final Query myTopPostsQuery = myRef.child("by_id");
        myTopPostsQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Note> notes = new ArrayList<Note>();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    String[] keywordSplit = keyword.split("%");
                    Note note = postSnapshot.getValue(Note.class);
                    for (String keywordStr : keywordSplit) {
                        if(TextUtils.isEmpty(keywordStr)) continue;
                        if (note.userName.contains(keywordStr) || note.note.contains(keywordStr)) {
                            notes.add(note);
                            break;
                        }
                    }
                }
                notesCallback.onDataAvailable(notes);
                myTopPostsQuery.removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                myTopPostsQuery.removeEventListener(this);
                if (notesCallback != null) {
                    notesCallback.onDataNotAvailable();
                }
            }
        });
    }

    @Override
    public void getLocations(String userName, final LocationsCallback locationsCallback) {
        final Query myTopPostsQuery = myRef.child("by_location");
        myTopPostsQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Coordinates> coordinates = new ArrayList<Coordinates>();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Coordinates coordinate = postSnapshot.child("coordinate").getValue(Coordinates.class);
                    coordinates.add(coordinate);
                }
                locationsCallback.onDataAvailable(coordinates);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                locationsCallback.onDataNotAvailable();
            }
        });
    }
}
