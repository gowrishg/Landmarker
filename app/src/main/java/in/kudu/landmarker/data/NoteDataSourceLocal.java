package in.kudu.landmarker.data;

import android.os.AsyncTask;

import java.util.List;

/**
 * Created by gowrishg on 16/10/17.
 */

public class NoteDataSourceLocal implements NoteDataSource {

    NoteDatabase mNoteDatabase;

    public NoteDataSourceLocal(NoteDatabase noteDatabase) {
        mNoteDatabase = noteDatabase;
    }

    @Override
    public void getNotes(String userName, NotesCallback notesCallback) {
        throw new UnsupportedOperationException("Yet to be implemented");
    }

    @Override
    public void getNotes(Coordinates coordinates, final String userName, final NotesCallback notesCallback) {
        new AsyncTask<Coordinates, Void, List<Note>>() {

            @Override
            protected List<Note> doInBackground(Coordinates... coord) {
                return mNoteDatabase.noteDao().getNotes(coord[0].latitude, coord[0].longitude, userName);
            }

            @Override
            protected void onPostExecute(List<Note> notes) {
                super.onPostExecute(notes);
                if (notes == null || notes.size() == 0) {
                    notesCallback.onDataNotAvailable();
                } else {
                    notesCallback.onDataAvailable(notes);
                }
            }
        }.execute(coordinates);
    }

    @Override
    public void getNotes(final Coordinates coordinates, final NotesCallback notesCallback) {
        new AsyncTask<Coordinates, Void, List<Note>>() {

            @Override
            protected List<Note> doInBackground(Coordinates... coord) {
                return mNoteDatabase.noteDao().getNotes(coord[0].latitude, coord[0].longitude);
            }

            @Override
            protected void onPostExecute(List<Note> notes) {
                super.onPostExecute(notes);
                if (notes == null || notes.size() == 0) {
                    notesCallback.onDataNotAvailable();
                } else {
                    notesCallback.onDataAvailable(notes);
                }
            }
        }.execute(coordinates);
    }

    @Override
    public void addNote(final Note... note) {
        new AsyncTask<Note, Void, Void>() {

            @Override
            protected Void doInBackground(Note... note) {
                mNoteDatabase.noteDao().addNote(note);
                return null;
            }
        }.execute(note);
    }

    @Override
    public void searchByNameOrNotes(String keyword, final NotesCallback notesCallback) {
        new AsyncTask<String, Void, List<Note>>() {

            @Override
            protected List<Note> doInBackground(String... keywordStrArr) {
                return mNoteDatabase.noteDao().searchByNameOrNotes(keywordStrArr[0]);
            }

            @Override
            protected void onPostExecute(List<Note> notes) {
                super.onPostExecute(notes);
                if (notes == null || notes.size() == 0) {
                    notesCallback.onDataNotAvailable();
                } else {
                    notesCallback.onDataAvailable(notes);
                }
            }
        }.execute(keyword);
    }

    @Override
    public void getLocations(String userName, final LocationsCallback locationsCallback) {
        new AsyncTask<String, Void, List<Coordinates>>() {

            @Override
            protected List<Coordinates> doInBackground(String... name) {
                return mNoteDatabase.noteDao().getLocations(name[0]);
            }

            @Override
            protected void onPostExecute(List<Coordinates> coordinates) {
                super.onPostExecute(coordinates);
                if (coordinates == null || coordinates.size() == 0) {
                    locationsCallback.onDataNotAvailable();
                } else {
                    locationsCallback.onDataAvailable(coordinates);
                }
            }
        }.execute(userName);
    }
}
