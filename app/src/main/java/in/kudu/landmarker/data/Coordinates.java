package in.kudu.landmarker.data;

/**
 * Created by gowrishg on 16/10/17.
 */
public class Coordinates {

    // latitude in degree decimal
    public double latitude;
    // longitude in degree decimal
    public double longitude;

    /**
     * Returns a key based on the lat/lon values
     * It's acceptable to round off coordinate to 6 digits
     * @return key
     */
    public String getUuid() {
        long roundOffLat = Math.round(latitude * 1000000);
        long roundOffLon = Math.round(longitude * 1000000);
        String str = roundOffLat + "_" + roundOffLon;
        return str;
    }
}
