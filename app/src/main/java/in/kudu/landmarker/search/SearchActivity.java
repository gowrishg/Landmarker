package in.kudu.landmarker.search;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import in.kudu.landmarker.R;
import in.kudu.landmarker.ViewModelFactory;
import in.kudu.landmarker.data.Note;
import in.kudu.landmarker.notes.NotesCustomAdapter;

public class SearchActivity extends AppCompatActivity {

    EditText searchEditText;
    ImageButton searchButton;
    SearchViewModel mSearchViewModel;
    Observer<List<Note>> searchResultsObserver;
    private NotesCustomAdapter mNotesCustomAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.inflateMenu(R.menu.main_menu);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ViewModelFactory factory = ViewModelFactory.getInstance(getApplication());
        mSearchViewModel = ViewModelProviders.of(this, factory).get(SearchViewModel.class);

        searchEditText = (EditText) findViewById(R.id.searchEditText);
        searchButton = (ImageButton) findViewById(R.id.searchButton);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performSearch();
            }
        });
        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch();
                    return true;
                }
                return false;
            }
        });

        RecyclerView notesRecyclerView = (RecyclerView) findViewById(R.id.notesRecyclerView);
        mNotesCustomAdapter = new NotesCustomAdapter();
        notesRecyclerView.setHasFixedSize(true);
        notesRecyclerView.setAdapter(mNotesCustomAdapter);
        notesRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        searchResultsObserver = new Observer<List<Note>>() {
            @Override
            public void onChanged(@Nullable List<Note> notes) {
                mNotesCustomAdapter.swapData(notes);
            }
        };

        mSearchViewModel.getNotes().observeForever(searchResultsObserver);
    }

    /**
     * Performs search
     */
    private void performSearch() {
        String search = searchEditText.getText().toString();
        if (mSearchViewModel.isValid(search)) {
            mSearchViewModel.searchNotes(search);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
