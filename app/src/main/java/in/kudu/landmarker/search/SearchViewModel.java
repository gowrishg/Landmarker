package in.kudu.landmarker.search;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import in.kudu.landmarker.data.Note;
import in.kudu.landmarker.data.NoteDataSource;
import in.kudu.landmarker.data.NoteRepository;

/**
 * Created by gowrishg on 17/10/17.
 */

public class SearchViewModel extends AndroidViewModel {
    private final NoteRepository mNoteRepository;
    MutableLiveData<List<Note>> notes;

    public SearchViewModel(Application application, NoteRepository noteRepository) {
        super(application);
        this.mNoteRepository = noteRepository;
    }

    /**
     * returns object of notes which may be observed by the UI components
     * @return list of notes
     */
    public MutableLiveData<List<Note>> getNotes() {
        if (notes == null) {
            notes = new MutableLiveData<>();
        }
        return notes;
    }

    /**
     * Search for notes based on the keyword.
     * The order of input string matters
     * @param keyword any string
     */
    public void searchNotes(String keyword) {
        mNoteRepository.search(keyword, new NoteDataSource.NotesCallback() {
            @Override
            public void onDataAvailable(List<Note> notes) {
                getNotes().postValue(notes);
            }

            @Override
            public void onDataNotAvailable() {
                getNotes().postValue(new ArrayList<Note>(0));
            }
        });
    }

    /**
     * Checks if the input is valid or not
     * @param search
     * @return true if it's valid
     */
    public boolean isValid(String search) {
        if (TextUtils.isEmpty(search)) {
            search = "";
        }
        return search.trim().length() > 0;
    }
}
