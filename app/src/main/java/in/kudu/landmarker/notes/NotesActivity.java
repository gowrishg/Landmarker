package in.kudu.landmarker.notes;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import java.util.List;

import in.kudu.landmarker.R;
import in.kudu.landmarker.ViewModelFactory;
import in.kudu.landmarker.data.Note;

public class NotesActivity extends AppCompatActivity {
    private static final String TAG = "CustomAdapter";
    NotesCustomAdapter mNotesCustomAdapter;
    NotesViewModel notesViewModel;
    Observer mNotesObserver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_black_24dp);

        RecyclerView notesRecyclerView = (RecyclerView) findViewById(R.id.notesRecyclerView);
        mNotesCustomAdapter = new NotesCustomAdapter();
        notesRecyclerView.setHasFixedSize(true);
        notesRecyclerView.setAdapter(mNotesCustomAdapter);
        notesRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        ViewModelFactory factory = ViewModelFactory.getInstance(getApplication());
        notesViewModel = ViewModelProviders.of(this, factory).get(NotesViewModel.class);

        //! gets the lat/lon from the bundle
        Bundle bundle = getIntent().getExtras();
        double lat = bundle.getDouble("latitude");
        double lon = bundle.getDouble("longitude");
        notesViewModel.setCurrentLocation(lat, lon);

        mNotesObserver = new Observer<List<Note>>() {
            @Override
            public void onChanged(@Nullable List<Note> notes) {
                mNotesCustomAdapter.swapData(notes);
            }
        };
        notesViewModel.getNotes().observeForever(mNotesObserver);
        notesViewModel.loadNotes();

    }

    @Override
    protected void onDestroy() {
        notesViewModel.getNotes().removeObserver(mNotesObserver);
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
