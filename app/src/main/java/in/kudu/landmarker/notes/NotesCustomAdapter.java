package in.kudu.landmarker.notes;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.kudu.landmarker.R;
import in.kudu.landmarker.data.Note;

/**
 * Created by gowrishg on 17/10/17.
 */
public class NotesCustomAdapter extends RecyclerView.Adapter<NotesCustomAdapter.ViewHolder> {
    private static final String TAG = "CustomAdapter";

    private List<Note> mNotes;

    /**
     * Swaps the new data with the older one
     * And updates the UI
     * @param notes
     */
    public void swapData(List<Note> notes) {
        if (notes == null)
            notes = new ArrayList<>();
        if (mNotes != null && mNotes.size() > 0)
            mNotes.clear();
        else
            mNotes = new ArrayList<>();
        mNotes.addAll(notes);
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView usernameTextView;
        private final TextView notesTextView;

        public ViewHolder(View v) {
            super(v);
            usernameTextView = v.findViewById(R.id.usernameTextView);
            notesTextView = v.findViewById(R.id.notesTextView);
        }

        public TextView getUsernameTextView() {
            return usernameTextView;
        }

        public TextView getNotesTextView() {
            return notesTextView;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_landmark_note, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Log.d(TAG, "Element " + position + " set.");
        holder.getNotesTextView().setText(mNotes.get(position).note);
        holder.getUsernameTextView().setText(mNotes.get(position).userName);
    }

    @Override
    public int getItemCount() {
        return mNotes == null ? 0 : mNotes.size();
    }

}