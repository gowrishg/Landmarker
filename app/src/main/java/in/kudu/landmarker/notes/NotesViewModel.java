package in.kudu.landmarker.notes;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import in.kudu.landmarker.data.Coordinates;
import in.kudu.landmarker.data.Note;
import in.kudu.landmarker.data.NoteDataSource;
import in.kudu.landmarker.data.NoteRepository;
import in.kudu.landmarker.data.UserRepository;

/**
 * Created by gowrishg on 17/10/17.
 */

public class NotesViewModel extends AndroidViewModel {
    private final NoteRepository mNoteRepository;
    private final UserRepository mUserRepository;
    MutableLiveData<List<Note>> notes;
    private Coordinates currentLocation;

    public NotesViewModel(Application application, UserRepository userRepository, NoteRepository noteRepository) {
        super(application);
        this.mNoteRepository = noteRepository;
        this.mUserRepository = userRepository;
    }

    /**
     * Returns reference of the notes object which can be observed by UI components.
     * @return notes
     */
    public MutableLiveData<List<Note>> getNotes() {
        if (notes == null) {
            notes = new MutableLiveData<>();
        }
        return notes;
    }

    /**
     * Remembers the lat/lon
     * @param lat in degree decimal
     * @param lon in degree decimal
     */
    public void setCurrentLocation(double lat, double lon) {
        currentLocation = new Coordinates();
        currentLocation.latitude = lat;
        currentLocation.longitude = lon;
    }

    /**
     * Prepares data to be shown in UI
     */
    public void loadNotes() {
        mNoteRepository.getNotes(currentLocation, new NoteDataSource.NotesCallback() {
            @Override
            public void onDataAvailable(List<Note> notes) {
                getNotes().postValue(notes);
            }

            @Override
            public void onDataNotAvailable() {
                getNotes().postValue(new ArrayList<Note>(0));
            }
        });
    }
}
