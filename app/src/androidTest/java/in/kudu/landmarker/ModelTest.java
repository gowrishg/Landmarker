package in.kudu.landmarker;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.location.Location;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import in.kudu.landmarker.data.Coordinates;
import in.kudu.landmarker.data.Note;
import in.kudu.landmarker.data.NoteDataSource;
import in.kudu.landmarker.data.NoteDataSourceLocal;
import in.kudu.landmarker.data.NoteDataSourceRemote;
import in.kudu.landmarker.data.NoteDatabase;
import in.kudu.landmarker.data.NoteRepository;
import in.kudu.landmarker.data.UserRepository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by gowrishg on 18/10/17.
 */

@RunWith(AndroidJUnit4.class)
public class ModelTest {

    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    Application app;
    UserRepository userRepository;
    NoteRepository noteRepository;
    DatabaseReference userDatabaseReference;
    DatabaseReference notesDatabaseReference;
    NoteDatabase noteDatabase;
    NoteDataSourceLocal noteDataSourceLocal;

    @Before
    public void setup() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        app = (Application) appContext.getApplicationContext();
        ViewModelFactory viewModelFactory = ViewModelFactory.getInstance(app);
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        userDatabaseReference = firebaseDatabase.getReference("test_user_cache");
        userRepository = new UserRepository(app, userDatabaseReference);
        noteDatabase = Room.databaseBuilder(app, NoteDatabase.class, "test_NoteDatabase.db").build();
        noteDataSourceLocal = new NoteDataSourceLocal(noteDatabase);
        notesDatabaseReference = firebaseDatabase.getReference("test_RemoteNoteDatabase");
        NoteDataSourceRemote noteDataSourceRemote = new NoteDataSourceRemote(userDatabaseReference);
        noteRepository = new NoteRepository(app, userRepository, noteDataSourceLocal, noteDataSourceRemote);
    }

    @After
    public void tearDown() {
        userDatabaseReference.removeValue();
        notesDatabaseReference.removeValue();
    }

    @Test
    public void test_notes_within_location() {
        Location loc = new Location("gps");
        loc.setLatitude(1.003);
        loc.setLongitude(103.003);
        Coordinates coordinates = new Coordinates();
        coordinates.latitude = loc.getLatitude();
        coordinates.longitude = loc.getLongitude();

        String fakeNote = UUID.randomUUID().toString();
        String fakeName = UUID.randomUUID().toString();
        List<Note> notes = new ArrayList<Note>();
        for (int i = 0; i < 10; i++) {
            Note n = new Note();
            n.id = "test" + i;
            n.coordinates = new Coordinates();
            n.coordinates.latitude = loc.getLatitude();
            n.coordinates.longitude = loc.getLongitude();
            n.userName = fakeName;
            n.note = fakeNote + " " + i;
            noteDataSourceLocal.addNote(n);
        }

        noteDataSourceLocal.getNotes(coordinates, new NoteDataSource.NotesCallback() {
            @Override
            public void onDataAvailable(List<Note> note) {
                assertTrue(note.size() >= 10);
            }

            @Override
            public void onDataNotAvailable() {
                assertTrue(false);

            }
        });

        coordinates = new Coordinates();
        coordinates.latitude = -1.123123;
        coordinates.longitude = -103.123123;
        noteDataSourceLocal.getNotes(coordinates, fakeName, new NoteDataSource.NotesCallback() {
            @Override
            public void onDataAvailable(List<Note> note) {
                assertEquals(note.size(), 0);
            }

            @Override
            public void onDataNotAvailable() {
                assertTrue(true);

            }
        });

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void test_locations() {
        Location loc = new Location("gps");
        loc.setLatitude(1.003);
        loc.setLongitude(103.003);
        Coordinates coordinates = new Coordinates();
        coordinates.latitude = loc.getLatitude();
        coordinates.longitude = loc.getLongitude();

        String fakeNote = UUID.randomUUID().toString();
        String fakeName = UUID.randomUUID().toString();
        List<Note> notes = new ArrayList<Note>();
        for (int i = 0; i < 10; i++) {
            Note n = new Note();
            n.id = "test" + i;
            n.coordinates = new Coordinates();
            n.coordinates.latitude = loc.getLatitude();
            n.coordinates.longitude = loc.getLongitude();
            n.userName = fakeName;
            n.note = fakeNote + " " + i;
            noteDataSourceLocal.addNote(n);
        }

        noteDataSourceLocal.getLocations(fakeName, new NoteDataSource.LocationsCallback() {
            @Override
            public void onDataAvailable(List<Coordinates> coordinates) {
                assertTrue(coordinates.size() >= 1);
            }

            @Override
            public void onDataNotAvailable() {
                assertTrue(false);

            }
        });

        noteDataSourceLocal.getLocations("DFAKFJLDSJF;ASDJF", new NoteDataSource.LocationsCallback() {
            @Override
            public void onDataAvailable(List<Coordinates> coordinates) {
                assertEquals(coordinates.size(), 0);
            }

            @Override
            public void onDataNotAvailable() {
                assertTrue(true);

            }
        });

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void test_search() {
        Location loc = new Location("gps");
        loc.setLatitude(1.003);
        loc.setLongitude(103.003);
        Coordinates coordinates = new Coordinates();
        coordinates.latitude = loc.getLatitude();
        coordinates.longitude = loc.getLongitude();

        String fakeNote = UUID.randomUUID().toString();
        String fakeName = UUID.randomUUID().toString();
        List<Note> notes = new ArrayList<Note>();
        for (int i = 0; i < 10; i++) {
            Note n = new Note();
            n.id = "test" + i;
            n.coordinates = new Coordinates();
            n.coordinates.latitude = loc.getLatitude();
            n.coordinates.longitude = loc.getLongitude();
            n.userName = fakeName;
            n.note = fakeNote + " " + i;
            noteDataSourceLocal.addNote(n);
        }

        noteDataSourceLocal.searchByNameOrNotes(fakeName, new NoteDataSource.NotesCallback() {

            @Override
            public void onDataAvailable(List<Note> notes) {
                assertTrue(notes.size() >= 1);
            }

            @Override
            public void onDataNotAvailable() {
                assertTrue(false);

            }
        });

        noteDataSourceLocal.searchByNameOrNotes("FOR TESTING: fdkslfjdaskf jadsfj ;adsjf ;kdlsjf a;dslfj a", new NoteDataSource.NotesCallback() {
            @Override
            public void onDataAvailable(List<Note> notes) {
                assertEquals(notes.size(), 0);
            }

            @Override
            public void onDataNotAvailable() {
                assertTrue(true);

            }
        });

        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
