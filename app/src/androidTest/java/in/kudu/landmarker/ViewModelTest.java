package in.kudu.landmarker;

import android.app.Application;
import android.arch.lifecycle.Observer;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.location.Location;
import android.support.annotation.Nullable;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import in.kudu.landmarker.data.Coordinates;
import in.kudu.landmarker.data.Note;
import in.kudu.landmarker.data.NoteDataSourceLocal;
import in.kudu.landmarker.data.NoteDataSourceRemote;
import in.kudu.landmarker.data.NoteDatabase;
import in.kudu.landmarker.data.NoteRepository;
import in.kudu.landmarker.data.User;
import in.kudu.landmarker.data.UserRepository;
import in.kudu.landmarker.landing.LandingViewModel;
import in.kudu.landmarker.maps.MapsViewModel;
import in.kudu.landmarker.notes.NotesViewModel;
import in.kudu.landmarker.search.SearchViewModel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by gowrishg on 18/10/17.
 */

@RunWith(AndroidJUnit4.class)
public class ViewModelTest {

    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    Application app;
    UserRepository userRepository;
    NoteRepository noteRepository;
    DatabaseReference userDatabaseReference;
    DatabaseReference notesDatabaseReference;
    NoteDatabase noteDatabase;

    @Before
    public void setup() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        app = (Application) appContext.getApplicationContext();
        in.kudu.landmarker.ViewModelFactory viewModelFactory = in.kudu.landmarker.ViewModelFactory.getInstance(app);
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        userDatabaseReference = firebaseDatabase.getReference("test_user_cache");
        userRepository = new UserRepository(app, userDatabaseReference);
        noteDatabase = Room.databaseBuilder(app, NoteDatabase.class, "test_NoteDatabase.db").build();
        NoteDataSourceLocal noteDataSourceLocal = new NoteDataSourceLocal(noteDatabase);
        notesDatabaseReference = firebaseDatabase.getReference("test_RemoteNoteDatabase");
        NoteDataSourceRemote noteDataSourceRemote = new NoteDataSourceRemote(userDatabaseReference);
        noteRepository = new NoteRepository(app, userRepository, noteDataSourceLocal, noteDataSourceRemote);
    }

    @After
    public void tearDown() {
        userDatabaseReference.removeValue();
        notesDatabaseReference.removeValue();
    }

    @Test
    public void validate_input_string() {
        LandingViewModel landingViewModel = new LandingViewModel(app, userRepository, noteRepository);
        assertTrue(!landingViewModel.isValid(""));
        assertTrue(!landingViewModel.isValid("   "));
        assertTrue(landingViewModel.isValid("  a "));
        assertTrue(landingViewModel.isValid("abbc "));
        assertTrue(landingViewModel.isValid(" abbc "));
        assertTrue(landingViewModel.isValid(" abbc"));
    }

    @Test
    public void validate_set_retrieval_username() {
        LandingViewModel landingViewModel = new LandingViewModel(app, userRepository, noteRepository);
        landingViewModel.setUserName(" gowrish");
        assertEquals(landingViewModel.getUser().name, "gowrish");
        landingViewModel.setUserName(" gowrish ");
        assertEquals(landingViewModel.getUser().name, "gowrish");
        landingViewModel.setUserName("gowrish ");
        assertEquals(landingViewModel.getUser().name, "gowrish");
        landingViewModel.setUserName("gowr ish ");
        assertEquals(landingViewModel.getUser().name, "gowr ish");
    }

    @Test
    public void test_location() {
        MapsViewModel mapsViewModel = new MapsViewModel(app, userRepository, noteRepository);
        assertFalse(mapsViewModel.isValidLocation());
        Location loc = new Location("gps");
        loc.setLatitude(1.3);
        loc.setLongitude(103.3);
        mapsViewModel.setCurrentLocation(loc);
        assertTrue(mapsViewModel.isValidLocation());
    }

    @Test
    public void test_add_notes() {
        MapsViewModel mapsViewModel = new MapsViewModel(app, userRepository, noteRepository);

        Location loc = new Location("gps");
        loc.setLatitude(1.3);
        loc.setLongitude(103.3);
        mapsViewModel.setCurrentLocation(loc);

        mapsViewModel.getLocations().observeForever(new Observer<List<Coordinates>>() {
            @Override
            public void onChanged(@Nullable List<Coordinates> coordinates) {
                assertEquals(coordinates.size(), 1);
            }
        });

        mapsViewModel.addNote("yo check this new note");
        mapsViewModel.addNote("yo check this is another note");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_search() {
        MapsViewModel mapsViewModel = new MapsViewModel(app, userRepository, noteRepository);

        Location loc = new Location("gps");
        loc.setLatitude(1.3);
        loc.setLongitude(103.3);

        String fakeNote = UUID.randomUUID().toString();
        String fakeName = UUID.randomUUID().toString();
        List<Note> notes = new ArrayList<Note>();
        for (int i = 0; i < 10; i++) {
            Note n = new Note();
            n.id = "test" + i;
            n.coordinates = new Coordinates();
            n.coordinates.latitude = loc.getLatitude();
            n.coordinates.longitude = loc.getLongitude();
            n.userName = "yet another user" + i;
            n.note = fakeNote + " " + i;
            mapsViewModel.setCurrentLocation(loc);
            User u = new User();
            u.name = fakeName + " " + i;
            userRepository.setUser(u);
            mapsViewModel.addNote(n.note);
        }

        SearchViewModel searchViewModel = new SearchViewModel(app, noteRepository);
        searchViewModel.getNotes().observeForever(new Observer<List<Note>>() {
            @Override
            public void onChanged(@Nullable List<Note> notes) {
                Log.d("LANDMARK_TEST", "" + notes.size());
                assertEquals(notes.size(), 10);
            }
        });
        searchViewModel.searchNotes(fakeName);
        searchViewModel.searchNotes(fakeNote);

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void test_notes_within_location() {
        MapsViewModel mapsViewModel = new MapsViewModel(app, userRepository, noteRepository);

        Location loc = new Location("gps");
        loc.setLatitude(1.3);
        loc.setLongitude(103.3);

        String fakeNote = UUID.randomUUID().toString();
        String fakeName = UUID.randomUUID().toString();
        List<Note> notes = new ArrayList<Note>();
        for (int i = 0; i < 10; i++) {
            Note n = new Note();
            n.id = "test" + i;
            n.coordinates = new Coordinates();
            n.coordinates.latitude = loc.getLatitude();
            n.coordinates.longitude = loc.getLongitude();
            n.userName = "yet another user" + i;
            n.note = fakeNote + " " + i;
            mapsViewModel.setCurrentLocation(loc);
            User u = new User();
            u.name = fakeName;
            userRepository.setUser(u);
            mapsViewModel.addNote(n.note);
        }

        NotesViewModel notesViewModel = new NotesViewModel(app, userRepository, noteRepository);
        notesViewModel.getNotes().observeForever(new Observer<List<Note>>() {
            @Override
            public void onChanged(@Nullable List<Note> notes) {
                Log.d("LANDMARK_TEST", "" + notes.size());
                assertTrue(notes.size() >= 1);
            }
        });
        notesViewModel.setCurrentLocation(loc.getLatitude(), loc.getLongitude());
        notesViewModel.loadNotes();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
