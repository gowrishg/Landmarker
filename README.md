# Tigerspike - Assignment Brief

You are to create a simple mobile application – working title: *Landmark Remark* - that allows users to save location based notes on a map. These notes can be displayed on the map where they were saved and viewed by the user that created the note as well as other users of the application. The application must demonstrate the functionality captured in the following user stories:
1. As a user (of the application) I can see my current location on a map
2. As a user I can save a short note at my current location
3. As a user I can see notes that I have saved at the location they were saved on the map
4. As a user I can see the location, text, and user-name of notes other users have saved
5. As a user I have the ability to search for a note based on contained text or user-name
Your application does not need to be pretty (i.e. artist designed) but you should aim to make it as functional and useable as possible within the given constraints.

![Landmark Marker](assets/ui-design.png)

## Task

* Brief approach
    * The application follows a MVVM architecture.
    * The repository abstracts functionality to retrieve data locally and then fetches data online.
    * The applicaiton displays markers on the landing screen, then quitely fetches locations of every other user (covers user story 4). 
    * Similarly for search it's performed locally to keep user engaged.
    * There is no explicit server component to optimise online search. So all the results are fetched first from the server and then filtered within the client app.
    * The source code includes unit testing of ViewModel and Model (db component).

* Persistent data
    * Stores all notes of the user locally (Sqlite3 + Room)
    * The application performs search by notes and username on local data.
    * User notes are stored remotely and fetched on the fly.
    * Firebase is used to stores all user data

* Technologies used
    * Realtime Database hosted by Firebase. https://console.firebase.google.com/u/0/project/landmarker-9b7a4/database/data
    * Source code uploaded to gitlab. https://gitlab.com/gowrishg/Landmarker.git
    
* Limitations
    * Most of the online search is done within the app; which means all the result sets are brought over and then filtered. It's not optimum and could cause high network usage. 
    * If the number of markers (locations) are too high; then the current approach will cause high network usage. Some form of pagination is required.
    * Since the solution doesn't consider authentication, it's easy for users to masquerade.
    * The orientation change scenario is not considered in the solution. 
    
* Time line (**around 15hr**)
    * UI Design - 1hr
    * Architectural design - 1hr
    * Implementation of UI and clean up - 7hr
    * Setup the firebase connectivity - 2hr
    * Documentation - 1hr
    * Unit testing + Junit + instrumentation testing - 3hr
    